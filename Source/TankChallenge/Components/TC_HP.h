

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TC_HP.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogHP, Log, All);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FParentDiedDelegate, AActor*, _DeathInstigator);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FDamageTakenDelegate, AActor*, DmgInstigator, float, Dmg, float, NewHP);

class APawn;



UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANKCHALLENGE_API UTC_HP : public UActorComponent
{
	GENERATED_BODY()

protected:
	/**
	* The current HP the character has
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TC_HPComponent - Vars", Replicated)
		float CurrentHP;

	/**
	* The Max HP the character has
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TC_HPComponent - Vars", Replicated)
		float MaxHP;

	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Sets default values for this component's properties
	UTC_HP();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void InitializeComponent();

	/**
	* The actor that owns this module
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TC_HPComponent - Vars")
		APawn* Parent;

	/**
	* A delegate that we will call once HP hits 0
	*/
	UPROPERTY(BlueprintAssignable)
		FParentDiedDelegate OnParentDied;

	/**
	* A delegate we call on every damage taken
	*/
	UPROPERTY(BlueprintAssignable)
		FDamageTakenDelegate OnDamageTaken;

	/**
	* A function to start everything correctly
	*/
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "TC_HPComponent - Functions")
		void SetupModule();


	/**
	* Get current HP
	*/
	UFUNCTION(BlueprintPure, Category = "TC_HPComponent - GetSet")
		float GetCurrentHP();

	/**
	* Set current HP
	*/
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "TC_HPComponent - GetSet")
		void SetCurrentHP(float newHP);

	/**
	* Get Max HP
	*/
	UFUNCTION(BlueprintPure, Category = "TC_HPComponent - GetSet")
		float GetMaxHP();

	/**
	* Set Max HP
	*/
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "TC_HPComponent - GetSet")
		void SetMaxHP(float newHP);

	/**
	* Our function that manages dealing damage.
	*/
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "TC_HPComponent - Functions")
		void TakeDamageFunction(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	/**
	* Kill Owner function
	*/
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "TC_HPComponent - Functions")
		void KillOwner(AActor* _DeathInstigator, float _Damage);

		
};
