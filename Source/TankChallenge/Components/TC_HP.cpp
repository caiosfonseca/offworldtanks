


#include "TC_HP.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/DamageType.h"
#include "GameFramework/Controller.h"
#include "Math/UnrealMathUtility.h"

DEFINE_LOG_CATEGORY(LogHP);

// Sets default values for this component's properties
UTC_HP::UTC_HP()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicated(true);

	// ...
}

void UTC_HP::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTC_HP, CurrentHP);
	DOREPLIFETIME(UTC_HP, MaxHP);
}


// Called when the game starts
void UTC_HP::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UTC_HP::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTC_HP::InitializeComponent()
{
	Super::InitializeComponent();
}

void UTC_HP::SetupModule_Implementation()
{
	//UE_LOG(LogHP, Warning, TEXT("Owner = %s"), *GetOwner()->GetName());
	Parent = Cast<APawn>(GetOwner());
	if (Parent)
	{
		// binding this on dedicated server caused a crash
		//Parent->OnTakeAnyDamage.AddDynamic(this, &UTC_HP::TakeDamageFunction);
		//UE_LOG(LogHP, Warning, TEXT("Binded damage function"));
	}
	CurrentHP = MaxHP;
}

float UTC_HP::GetCurrentHP()
{
	return CurrentHP;
}

void UTC_HP::SetCurrentHP_Implementation(float newHP)
{
	CurrentHP = newHP;
}

float UTC_HP::GetMaxHP()
{
	return MaxHP;
}

void UTC_HP::SetMaxHP_Implementation(float newHP)
{
	MaxHP = newHP;
}


void UTC_HP::TakeDamageFunction_Implementation(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	APawn* DamagedChar = Cast<APawn>(DamagedActor);
	if (DamagedChar)
	{
		CurrentHP = FMath::Clamp(CurrentHP - Damage, 0.0f, MaxHP);
		OnDamageTaken.Broadcast(DamageCauser, Damage, CurrentHP);
		//UE_LOG(LogHP, Warning, TEXT("%s Received %f damage of type %s from %s - Current HP = %f"), *DamagedActor->GetName(), Damage, *DamageType->GetName(), *DamageCauser->GetName(), CurrentHP);

		if (CurrentHP == 0)
		{
			KillOwner(DamageCauser, Damage);
		}
	}
}

void UTC_HP::KillOwner_Implementation(AActor* _DeathInstigator, float _Damage)
{
	if (Parent != nullptr)
	{
		//UE_LOG(LogHP, Warning, TEXT("Kill owner"));
		OnParentDied.Broadcast(_DeathInstigator);
	}
}