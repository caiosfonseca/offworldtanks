# Offworld Defense Simulations VanHackathon Project

## Project

Using the supplied project template, design and implement a simple match-based
multiplayer game oriented around tank combat. Implement the appropriate
gameplay framework (and other supporting) classes to create this game.


## Constraints

* Track damage/health.
* Track scores (kills/deaths/assists).
* End and restart match once a certain number of points have been reached.
* Create an “Objective” class, in which when tanks stand nearby it ticks up
  for each team or player. Once a limit is reached, the match ends with the
  top player/team winning.

## Rules
	
* Satisfies constraints section above.
* Level of effort exceeding that found in Epic’s starter templates.
* Must use Unreal 4.23.
* Must use Vanilla Unreal Engine, without any third party plugins or assets.
* Must use only built-in or provided assets.
* Finished project to be uploaded to submitter’s choice of providers as a zip file.
* Must demonstrate an understanding of multiplayer programming within Unreal:
    * Network game modes and mechanics
    * Client/Server architecture (project should work in dedicated server mode).
    * Multiplayer replication of states and events.

## Evaluation Criteria
* Correctness - Your project should be reasonably free from bugs or unintended behaviours.
* Architecture - The code should be maintainable and easy for others to follow.
* Effort - Submissions showing passion will stand out more than those doing the minimum.

## Bonus Points!
* C++ highly desirable
* Interesting mechanics
* VR
* Main Menu
* User interface
* Artificial Intelligence
