// Copyright (c) 2019, Offworld Industries.


#include "VehicleMovementNWheeledVehicle.h"
#include "WheeledVehicleMovementComponentNW.h"

AVehicleMovementNWheeledVehicle::AVehicleMovementNWheeledVehicle(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UWheeledVehicleMovementComponentNW>(VehicleMovementComponentName))
{

}
