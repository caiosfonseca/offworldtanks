// Copyright (c) 2019, Offworld Industries.


#include "VehicleMovementTankVehicle.h"
#include "WheeledVehicleMovementComponentTank.h"

AVehicleMovementTankVehicle::AVehicleMovementTankVehicle(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UWheeledVehicleMovementComponentTank>(VehicleMovementComponentName))
{

}
