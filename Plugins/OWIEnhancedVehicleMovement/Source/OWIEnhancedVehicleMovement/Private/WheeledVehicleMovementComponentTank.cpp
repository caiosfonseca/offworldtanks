// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "WheeledVehicleMovementComponentTank.h"
#include "PhysicsPublic.h"
#include "PhysXPublic.h"
#include "PhysXVehicleManager.h"
#include "Engine/Engine.h"
#include "Components/PrimitiveComponent.h"

UWheeledVehicleMovementComponentTank::UWheeledVehicleMovementComponentTank(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// grab default values from physx
	PxVehicleEngineData DefEngineData;
	EngineSetup.MOI = DefEngineData.mMOI;
	EngineSetup.MaxRPM = OmegaToRPM(DefEngineData.mMaxOmega);
	EngineSetup.DampingRateFullThrottle = DefEngineData.mDampingRateFullThrottle;
	EngineSetup.DampingRateZeroThrottleClutchEngaged = DefEngineData.mDampingRateZeroThrottleClutchEngaged;
	EngineSetup.DampingRateZeroThrottleClutchDisengaged = DefEngineData.mDampingRateZeroThrottleClutchDisengaged;

	// Convert from PhysX curve to ours
	FRichCurve* TorqueCurveData = EngineSetup.TorqueCurve.GetRichCurve();
	for (PxU32 KeyIdx = 0; KeyIdx < DefEngineData.mTorqueCurve.getNbDataPairs(); ++KeyIdx)
	{
		float Input = DefEngineData.mTorqueCurve.getX(KeyIdx) * EngineSetup.MaxRPM;
		float Output = DefEngineData.mTorqueCurve.getY(KeyIdx) * DefEngineData.mPeakTorque;
		TorqueCurveData->AddKey(Input, Output);
	}

	PxVehicleClutchData DefClutchData;
	TransmissionSetup.ClutchStrength = DefClutchData.mStrength;

	PxVehicleGearsData DefGearSetup;
	TransmissionSetup.GearSwitchTime = DefGearSetup.mSwitchTime;
	TransmissionSetup.ReverseGearRatio = DefGearSetup.mRatios[PxVehicleGearsData::eREVERSE];
	TransmissionSetup.FinalRatio = DefGearSetup.mFinalRatio;

	PxVehicleAutoBoxData DefAutoBoxSetup;
	TransmissionSetup.NeutralGearUpRatio = DefAutoBoxSetup.mUpRatios[PxVehicleGearsData::eNEUTRAL];
	TransmissionSetup.NeutralGearDownRatio = DefAutoBoxSetup.mDownRatios[PxVehicleGearsData::eNEUTRAL];
	TransmissionSetup.GearAutoBoxLatency = DefAutoBoxSetup.getLatency();
	TransmissionSetup.bUseGearAutoBox = true;

	FVehicleTankGearData DefaultReverseGear;
	DefaultReverseGear.Ratio = TransmissionSetup.ReverseGearRatio;
	DefaultReverseGear.UpRatio = 0.9f;
	DefaultReverseGear.DownRatio = 0.3f;
	TransmissionSetup.BackwardGears.Add(DefaultReverseGear);

	for (uint32 i = PxVehicleGearsData::eFIRST; i < DefGearSetup.mNbRatios; ++i)
	{
		FVehicleTankGearData GearData;
		GearData.DownRatio = DefAutoBoxSetup.mDownRatios[i];
		GearData.UpRatio = DefAutoBoxSetup.mUpRatios[i];
		GearData.Ratio = DefGearSetup.mRatios[i];
		TransmissionSetup.ForwardGears.Add(GearData);
	}

	// Initialize WheelSetups array with 4 wheels
	WheelSetups.SetNum(4);

	//bReverseAsBrake = false;
	bLeftTrackLocked = false;
	bRightTrackLocked = false;
}

#if WITH_EDITOR
void UWheeledVehicleMovementComponentTank::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	const FName PropertyName = PropertyChangedEvent.Property ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if (PropertyName == TEXT("DownRatio"))
	{
		for (int32 GearIdx = 0; GearIdx < TransmissionSetup.ForwardGears.Num(); ++GearIdx)
		{
			FVehicleTankGearData& GearData = TransmissionSetup.ForwardGears[GearIdx];
			GearData.DownRatio = FMath::Min(GearData.DownRatio, GearData.UpRatio);
		}
	}
	else if (PropertyName == TEXT("UpRatio"))
	{
		for (int32 GearIdx = 0; GearIdx < TransmissionSetup.ForwardGears.Num(); ++GearIdx)
		{
			FVehicleTankGearData& GearData = TransmissionSetup.ForwardGears[GearIdx];
			GearData.UpRatio = FMath::Max(GearData.DownRatio, GearData.UpRatio);
		}
	}
}
#endif

float FVehicleTankEngineData::FindPeakTorque() const
{
	// Find max torque
	float PeakTorque = 0.0f;
	TArray<FRichCurveKey> TorqueKeys = TorqueCurve.GetRichCurveConst()->GetCopyOfKeys();
	for (int32 KeyIdx = 0; KeyIdx < TorqueKeys.Num(); ++KeyIdx)
	{
		FRichCurveKey& Key = TorqueKeys[KeyIdx];
		PeakTorque = FMath::Max(PeakTorque, Key.Value);
	}
	return PeakTorque;
}

static void GetVehicleEngineSetup(const FVehicleTankEngineData& Setup, PxVehicleEngineData& PxSetup)
{
	PxSetup.mMOI = M2ToCm2(Setup.MOI);
	PxSetup.mMaxOmega = RPMToOmega(Setup.MaxRPM);
	PxSetup.mDampingRateFullThrottle = M2ToCm2(Setup.DampingRateFullThrottle);
	PxSetup.mDampingRateZeroThrottleClutchEngaged = M2ToCm2(Setup.DampingRateZeroThrottleClutchEngaged);
	PxSetup.mDampingRateZeroThrottleClutchDisengaged = M2ToCm2(Setup.DampingRateZeroThrottleClutchDisengaged);

	float PeakTorque = Setup.FindPeakTorque(); // In Nm
	PxSetup.mPeakTorque = M2ToCm2(PeakTorque);	// convert Nm to (kg cm^2/s^2)

	// Convert from our curve to PhysX
	PxSetup.mTorqueCurve.clear();
	TArray<FRichCurveKey> TorqueKeys = Setup.TorqueCurve.GetRichCurveConst()->GetCopyOfKeys();
	int32 NumTorqueCurveKeys = FMath::Min<int32>(TorqueKeys.Num(), PxVehicleEngineData::eMAX_NB_ENGINE_TORQUE_CURVE_ENTRIES);
	for (int32 KeyIdx = 0; KeyIdx < NumTorqueCurveKeys; ++KeyIdx)
	{
		FRichCurveKey& Key = TorqueKeys[KeyIdx];
		PxSetup.mTorqueCurve.addPair(FMath::Clamp(Key.Time / Setup.MaxRPM, 0.0f, 1.0f), Key.Value / PeakTorque); // Normalize torque to 0-1 range
	}
}

static void GetVehicleGearSetup(const FVehicleTankTransmissionData& Setup, PxVehicleGearsData& PxSetup)
{
	// ensure back and fwd gear is less than 30 total
	int32 NumBackGears = Setup.BackwardGears.Num() - 1;
	for (int32 i = 0; i < Setup.BackwardGears.Num(); ++i)
	{
		PxSetup.mRatios[PxVehicleGearsData::eREVERSE - i] = Setup.BackwardGears[i].Ratio;
	}
	PxSetup.mRatios[PxVehicleGearsData::eNEUTRAL] = 0.0f;
	PxSetup.mSwitchTime = Setup.GearSwitchTime;
	for (int32 i = 0; i < Setup.ForwardGears.Num(); ++i)
	{
		PxSetup.mRatios[i + PxVehicleGearsData::eFIRST] = Setup.ForwardGears[i].Ratio;
	}
	PxSetup.mFinalRatio = Setup.FinalRatio;
	PxSetup.mNbRatios = Setup.BackwardGears.Num() + Setup.ForwardGears.Num() + 1; // 1 for neutral gear;
}

static void GetVehicleAutoBoxSetup(const FVehicleTankTransmissionData& Setup, PxVehicleAutoBoxData& PxSetup)
{
	ensureMsgf(Setup.BackwardGears.Num() <= 13, TEXT("Too many backward gears"));
	for (int32 i = 0; i < Setup.BackwardGears.Num(); ++i)
	{
		const FVehicleTankGearData& GearData = Setup.BackwardGears[i];
		PxSetup.mUpRatios[PxVehicleGearsData::eREVERSE - i] = GearData.UpRatio;
		PxSetup.mDownRatios[PxVehicleGearsData::eREVERSE - i] = GearData.DownRatio;
	}
	PxSetup.mUpRatios[PxVehicleGearsData::eNEUTRAL] = Setup.NeutralGearUpRatio;
	PxSetup.mDownRatios[PxVehicleGearsData::eNEUTRAL] = Setup.NeutralGearDownRatio;
	PxSetup.setLatency(Setup.GearAutoBoxLatency);
	for (int32 i = 0; i < Setup.ForwardGears.Num(); ++i)
	{
		const FVehicleTankGearData& GearData = Setup.ForwardGears[i];
		PxSetup.mUpRatios[i + PxVehicleGearsData::eFIRST] = GearData.UpRatio;
		PxSetup.mDownRatios[i + PxVehicleGearsData::eFIRST] = GearData.DownRatio;
	}
}

int32 UWheeledVehicleMovementComponentTank::GetCustomGearBoxNumForwardGears() const
{
	return TransmissionSetup.ForwardGears.Num();
}

int32 UWheeledVehicleMovementComponentTank::GetCustomGearBoxNumBackwardGears() const
{
	return TransmissionSetup.BackwardGears.Num();
}

void SetupDriveHelper(const UWheeledVehicleMovementComponentTank* VehicleData, const PxVehicleWheelsSimData* PWheelsSimData, PxVehicleDriveSimData& DriveData)
{
	PxVehicleEngineData EngineSetup;
	GetVehicleEngineSetup(VehicleData->EngineSetup, EngineSetup);
	DriveData.setEngineData(EngineSetup);

	PxVehicleClutchData ClutchSetup;
	ClutchSetup.mStrength = M2ToCm2(VehicleData->TransmissionSetup.ClutchStrength);
	DriveData.setClutchData(ClutchSetup);

	PxVehicleGearsData GearSetup;
	GetVehicleGearSetup(VehicleData->TransmissionSetup, GearSetup);
	DriveData.setGearsData(GearSetup);

	PxVehicleAutoBoxData AutoBoxSetup;
	GetVehicleAutoBoxSetup(VehicleData->TransmissionSetup, AutoBoxSetup);
	DriveData.setAutoBoxData(AutoBoxSetup);
}

void UWheeledVehicleMovementComponentTank::SetupVehicle()
{
	if (!UpdatedPrimitive)
	{
		return;
	}

	if (WheelSetups.Num() < 4)
	{
		PVehicle = nullptr;
		PVehicleDrive = nullptr;
		return;
	}

	for (int32 WheelIdx = 0; WheelIdx < WheelSetups.Num(); ++WheelIdx)
	{
		const FWheelSetup& WheelSetup = WheelSetups[WheelIdx];
		if (WheelSetup.BoneName == NAME_None)
		{
			return;
		}
	}

	// Setup the chassis and wheel shapes
	SetupVehicleShapes();

	// Setup mass properties
	SetupVehicleMass();

	// Setup the wheels
	PxVehicleWheelsSimData* PWheelsSimData = PxVehicleWheelsSimData::allocate(WheelSetups.Num());
	SetupWheels(PWheelsSimData);

	// Setup drive data
	PxVehicleDriveSimData DriveData;
	SetupDriveHelper(this, PWheelsSimData, DriveData);

	// Create the vehicle
	PxVehicleDriveTank* PVehicleDriveTank = PxVehicleDriveTank::allocate(WheelSetups.Num());
	check(PVehicleDriveTank);

	FBodyInstance* TargetInstance = UpdatedPrimitive->GetBodyInstance();

	FPhysicsCommand::ExecuteWrite(TargetInstance->ActorHandle, [&](const FPhysicsActorHandle& Actor)
	{
		PxRigidActor* PActor = FPhysicsInterface::GetPxRigidActor_AssumesLocked(Actor);
		if (!PActor)
		{
			return;
		}

		if (PxRigidDynamic* PVehicleActor = PActor->is<PxRigidDynamic>())
		{
			PVehicleDriveTank->setup(GPhysXSDK, PVehicleActor, *PWheelsSimData, DriveData, 0);
			PVehicleDriveTank->setToRestState();
			PVehicleDriveTank->setDriveModel(PxVehicleDriveTankControlModel::eSPECIAL);
			//TODO: add parameter to support both of these
			//PVehicleDriveTank->setDriveModel(PxVehicleDriveTankControlModel::eSTANDARD);

			// cleanup
			PWheelsSimData->free();
		}
	});

	PWheelsSimData = nullptr;

	// cache values
	PVehicle = PVehicleDriveTank;
	PVehicleDrive = PVehicleDriveTank;


	SetUseAutoGears(TransmissionSetup.bUseGearAutoBox);
}

template<class T>
static CONSTEXPR FORCEINLINE T Sign(const T A)
{
	return (A >= (T)0) ? (T)1 : (T)-1;
}

void UWheeledVehicleMovementComponentTank::UpdateSimulation(float DeltaTime)
{
	if (PVehicleDrive == nullptr)
	{
		return;
	}

	FBodyInstance* TargetInstance = UpdatedPrimitive->GetBodyInstance();

	FPhysicsCommand::ExecuteWrite(TargetInstance->ActorHandle, [&](const FPhysicsActorHandle& Actor)
	{
		PxVehicleDriveTankRawInputData RawInputData(PxVehicleDriveTankControlModel::eSPECIAL);
		//PxVehicleDriveTankRawInputData RawInputData(PxVehicleDriveTankControlModel::eSTANDARD);

		if (BrakeInput > 0.0001f)
		{
			RawInputData.setAnalogLeftBrake(BrakeInput);
			RawInputData.setAnalogRightBrake(BrakeInput);
		}


		//Set the steering inputs based upon which mode of driving we are using for this vehicle
		float LeftSteeringInput = 0.0f;
		float RightSteeringInput = 0.0f;

		//If the user is changing direction we should really be braking first and not applying any gas, so wait until they've changed gears
		if ((GetCurrentGear() > 0 && GetTargetGear() < 0) || (GetCurrentGear() < 0 && GetTargetGear() > 0))
		{
			RawInputData.setAnalogLeftBrake(FMath::Abs(ThrottleInput));
			RawInputData.setAnalogRightBrake(FMath::Abs(ThrottleInput));
		}

		//Check if we are trying to pivot in a circle
		if (/*GetForwardSpeed() < 1000.0f &&*/ GetCurrentGear() > 0 && !FMath::IsNearlyZero(SteeringInput, 0.01f))
		{
			//Force the target gear to 1st gear, improves steering control
			//SetTargetGear(1, true);
			//There is no throttle but all in the steering, so pivot
			RawInputData.setAnalogAccel(FMath::Abs(SteeringInput));
			//SPECIAL:
			//LeftSteeringInput = SteeringInput;
			//RightSteeringInput = -1.0f * SteeringInput;
			if (SteeringInput < 0.0f)
			{
				//Pivoting left
				LeftSteeringInput = -1.0f;
				RightSteeringInput = 1.0f;// *SteeringInput;
				//RawInputData.setAnalogLeftBrake(-SteeringInput);

				if (HandbrakeInput > 0.0001f)
				{
					RawInputData.setAnalogLeftBrake(HandbrakeInput);
				}
			}
			else
			{
				//Pivoting right
				LeftSteeringInput = 1.0f;// SteeringInput;
				RightSteeringInput = -1.0f;
				if (HandbrakeInput > 0.0001f)
				{
					RawInputData.setAnalogRightBrake(HandbrakeInput);
				}
				//RawInputData.setAnalogRightBrake(SteeringInput);
			}
			//TRACE("!!!!!!!!!!!!!PIVOT TURN!!!!!!!!!!!!!");
		}
		else
		{
			//Apply the handbrake if user is applying it
			if (HandbrakeInput > 0.0001f)
			{
				RawInputData.setAnalogLeftBrake(HandbrakeInput);
				RawInputData.setAnalogRightBrake(HandbrakeInput);
			}

			//Set the throttle to the proper value
			RawInputData.setAnalogAccel(ThrottleInput);
			//Check if we are trying to steer
			if (FMath::IsNearlyZero(SteeringInput, 0.01f))
			{
				//We are not trying to steer, so put full power to each track
				LeftSteeringInput = 1.0f;
				RightSteeringInput = 1.0f;
			}
			else if (SteeringInput < 0.0f)
			{
				//Steering to the left (SteeringInput is negative, slow down left track, speed up right track)
				//STANDARD:
				//LeftSteeringInput = 1.0f + SteeringInput;
				//RightSteeringInput = 0.0f - SteeringInput;
				//RawInputData.setAnalogLeftBrake(-SteeringInput);
				//SPECIAL:
				LeftSteeringInput = SteeringInput;
				RightSteeringInput = -1.0f * SteeringInput;
				//Check if we have any throttle
				if (FMath::IsNearlyZero(ThrottleInput, 0.01f))
				{
					//We need some throttle to steer
					RawInputData.setAnalogAccel(-SteeringInput);
				}
			}
			else if (SteeringInput > 0.0f)
			{
				//Steering to the right (SteeringInput is positive, speed up left track, slow down right track)
				//STANDARD:
				//LeftSteeringInput = SteeringInput;
				//RightSteeringInput = 1.0f - SteeringInput;
				//RawInputData.setAnalogRightBrake(SteeringInput);
				//SPECIAL:
				LeftSteeringInput = SteeringInput;
				RightSteeringInput = -1.0f * SteeringInput;
				//Check if we have any throttle
				if (FMath::IsNearlyZero(ThrottleInput, 0.01f))
				{
					//We need some throttle to steer
					RawInputData.setAnalogAccel(SteeringInput);
				}
			}
		}

		RawInputData.setAnalogLeftThrust(LeftSteeringInput);
		RawInputData.setAnalogRightThrust(RightSteeringInput);

		//if (!FMath::IsNearlyZero(ThrottleInput + SteeringInput, 0.0001f))
		//{
		//	UE_LOG(LogPhysics, Warning, TEXT("Th:%4.1f St:%4.1f LS:%4.1f RS:%4.1f BI:%4.1f"), ThrottleInput, SteeringInput, LeftSteeringInput, RightSteeringInput, BrakeInput);
		//}
		//const float AA = RawInputData.getAnalogAccel();
		//const float LT = RawInputData.getAnalogLeftThrust();
		//const float RT = RawInputData.getAnalogRightThrust();
		//const float LB = RawInputData.getAnalogLeftBrake();
		//const float RB = RawInputData.getAnalogRightBrake();
		//if (!FMath::IsNearlyZero(AA + LB + RB, 0.0001f))
		//{
		//	UE_LOG(LogPhysics, Warning, TEXT("AA:%4.1f LT:%4.1f RT:%4.1f LB:%4.1f RB:%4.1f"), AA, LT, RT, LB, RB);
		//}

		// If left track broken, apply brakes to it
		if(bLeftTrackLocked)
		{
			RawInputData.setAnalogLeftBrake(1.0f);
		}
		if(bRightTrackLocked)
		{
			RawInputData.setAnalogRightBrake(1.0f);
		}

		if (!GetUseAutoGears())
		{
			RawInputData.setGearUp(bRawGearUpInput);
			RawInputData.setGearDown(bRawGearDownInput);
		}

		PxVehiclePadSmoothingData SmoothData = {
//			{ ThrottleInputRate.RiseRate, BrakeInputRate.RiseRate, HandbrakeInputRate.RiseRate, SteeringInputRate.RiseRate, SteeringInputRate.RiseRate },
//			{ ThrottleInputRate.FallRate, BrakeInputRate.FallRate, HandbrakeInputRate.FallRate, SteeringInputRate.FallRate, SteeringInputRate.FallRate }
			//Acceleration					Left Brake					Right Brake					Left Thrust					Right Thrust
			{ ThrottleInputRate.RiseRate, HandbrakeInputRate.RiseRate, HandbrakeInputRate.RiseRate, SteeringInputRate.RiseRate, SteeringInputRate.RiseRate },
			{ ThrottleInputRate.FallRate, HandbrakeInputRate.FallRate, HandbrakeInputRate.FallRate, SteeringInputRate.FallRate, SteeringInputRate.FallRate }
		};

		PxVehicleDriveTank* PxVehicleTank = (PxVehicleDriveTank*)PVehicleDrive;
		PxVehicleDriveTankSmoothAnalogRawInputsAndSetAnalogInputs(SmoothData, RawInputData, DeltaTime, *PxVehicleTank);
	});

	//Compute the speed/velocity of each track
	CalculateSpeedOfEachTrack(DeltaTime, LeftTrackSpeed, RightTrackSpeed);
	//if (!FMath::IsNearlyZero(LeftTrackSpeed + RightTrackSpeed, 1e-8f))
	//{
	//	UE_LOG(LogPhysics, Warning, TEXT("TrackSpeed: %f | %f"), LeftTrackSpeed, RightTrackSpeed)
	//}
}

void UWheeledVehicleMovementComponentTank::UpdateEngineSetup(const FVehicleTankEngineData& NewEngineSetup)
{
	if (PVehicleDrive)
	{
		PxVehicleEngineData EngineData;
		GetVehicleEngineSetup(NewEngineSetup, EngineData);

		PxVehicleDriveTank* PVehicleDriveTank = (PxVehicleDriveTank*)PVehicleDrive;
		PVehicleDriveTank->mDriveSimData.setEngineData(EngineData);
	}
}

void UWheeledVehicleMovementComponentTank::UpdateTransmissionSetup(const FVehicleTankTransmissionData& NewTransmissionSetup)
{
	if (PVehicleDrive)
	{
		PxVehicleGearsData GearData;
		GetVehicleGearSetup(NewTransmissionSetup, GearData);

		PxVehicleAutoBoxData AutoBoxData;
		GetVehicleAutoBoxSetup(NewTransmissionSetup, AutoBoxData);

		PxVehicleDriveTank* PVehicleDriveTank = (PxVehicleDriveTank*)PVehicleDrive;
		PVehicleDriveTank->mDriveSimData.setGearsData(GearData);
		PVehicleDriveTank->mDriveSimData.setAutoBoxData(AutoBoxData);
	}
}

void UWheeledVehicleMovementComponentTank::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);
	if (Ar.IsLoading() && Ar.UE4Ver() < VER_UE4_VEHICLES_UNIT_CHANGE)
	{
		PxVehicleEngineData DefEngineData;
		const float DefaultRPM = OmegaToRPM(DefEngineData.mMaxOmega);

		//we need to convert from old units to new. This backwards compatable code fails in the rare case that they were using very strange values that are the new defaults in the correct units.
		EngineSetup.MaxRPM = EngineSetup.MaxRPM != DefaultRPM ? OmegaToRPM(EngineSetup.MaxRPM) : DefaultRPM;	//need to convert from rad/s to RPM
	}

	if (Ar.IsLoading() && Ar.UE4Ver() < VER_UE4_VEHICLES_UNIT_CHANGE2)
	{
		PxVehicleEngineData DefEngineData;
		PxVehicleClutchData DefClutchData;

		//we need to convert from old units to new. This backwards compatable code fails in the rare case that they were using very strange values that are the new defaults in the correct units.
		// BackwardsConvertCm2ToM2(EngineSetup.DampingRateFullThrottle, DefEngineData.mDampingRateFullThrottle);
		// BackwardsConvertCm2ToM2(EngineSetup.DampingRateZeroThrottleClutchDisengaged, DefEngineData.mDampingRateZeroThrottleClutchDisengaged);
		// BackwardsConvertCm2ToM2(EngineSetup.DampingRateZeroThrottleClutchEngaged, DefEngineData.mDampingRateZeroThrottleClutchEngaged);
		// BackwardsConvertCm2ToM2(EngineSetup.MOI, DefEngineData.mMOI);
		// BackwardsConvertCm2ToM2(TransmissionSetup.ClutchStrength, DefClutchData.mStrength);
	}
}

void UWheeledVehicleMovementComponentTank::ComputeConstants()
{
	Super::ComputeConstants();
	MaxEngineRPM = EngineSetup.MaxRPM;
}

void UWheeledVehicleMovementComponentTank::SetBothTracksThrottleInput(float InThrottle)
{
	RawBothTracksThrottleInput = InThrottle;
}

void UWheeledVehicleMovementComponentTank::SetLeftTrackThrottleInput(float InThrottle)
{
	RawLeftTrackThrottleInput = InThrottle;
}

void UWheeledVehicleMovementComponentTank::SetRightTrackThrottleInput(float InThrottle)
{
	RawRightTrackThrottleInput = InThrottle;
}

void UWheeledVehicleMovementComponentTank::CalculateSpeedOfEachTrack(float DeltaTime, float& OutCalcLeftTrackSpeed, float& OutCalcRightTrackSpeed)
{
	FPhysXVehicleManager* MyVehicleManager = FPhysXVehicleManager::GetVehicleManagerFromScene(GetWorld()->GetPhysicsScene());
	SCOPED_SCENE_READ_LOCK(MyVehicleManager->GetScene());

	//Compute the speed/velocity of each track
	PxReal LeftTrackTotal = 0.0f;
	PxReal RightTrackTotal = 0.0f;
	const uint32 PNumWheels = PVehicle->mWheelsSimData.getNbWheels();
	int32 NumWheels = PNumWheels;
	if (WheelSpeeds.Num() != PNumWheels)
	{
		WheelSpeeds.Reset(NumWheels);
		WheelSpeeds.AddZeroed(NumWheels);
	}

	if (WheelAngles.Num() != PNumWheels)
	{
		WheelAngles.Reset(NumWheels);
		WheelAngles.AddZeroed(NumWheels);
	}

	for (uint32 w = 0; w < PNumWheels; ++w)
	{
		const PxReal WheelRadius = PVehicle->mWheelsSimData.getWheelData(w).mRadius;
		const PxReal WheelRotation = FMath::Fmod(PVehicle->mWheelsDynData.getWheelRotationAngle(w), 2.0f * PI);
		const float LastRotation = WheelAngles[w];
		WheelAngles[w] = WheelRotation;
		float WheelRotDelta = WheelRotation - LastRotation;
		if (WheelRotDelta > PI)
		{
			WheelRotDelta = WheelRotation - (LastRotation + 2.0f * PI);
		}
		else if (WheelRotDelta < -PI)
		{
			WheelRotDelta = (WheelRotation + 2.0f * PI) - LastRotation;
		}
		//UE_LOG(LogPhysics, Warning, TEXT("WheelRotDelta[%d]: WRD:%f WR:%f LR:%f"), w, WheelRotDelta, WheelRotation, LastRotation);
		const float LastSpeed = WheelSpeeds[w];
		const float CurrentSpeed = (WheelRotDelta * WheelRadius) / DeltaTime;
		WheelSpeeds[w] = (LastSpeed + CurrentSpeed) / 2.0f; //Mean of previous and current speeds to try to smooth out any spikes
		//const PxReal WheelSpeed = PVehicle->mWheelsDynData.getWheelRotationSpeed(w) * WheelRadius;
		if (w % 2 == 0)
		{
			//This is for the left track
			LeftTrackTotal += WheelSpeeds[w];//WheelSpeed;
		}
		else
		{
			//This is for the right track
			RightTrackTotal += WheelSpeeds[w];//WheelSpeed;
		}
	}
	OutCalcLeftTrackSpeed = LeftTrackTotal / (((float)PNumWheels) / 2.0f);
	OutCalcRightTrackSpeed = RightTrackTotal / (((float)PNumWheels) / 2.0f);
	//FString AllWheelAngles;
	//for (float f : WheelAngles)
	//{
	//	AllWheelAngles.Append(FString::Printf(TEXT(" %03.1f |"), f));
	//}
	//FString AllWheelSpeeds;
	//for (float f : WheelSpeeds)
	//{
	//	AllWheelSpeeds.Append(FString::Printf(TEXT(" %03.1f |"), f));
	//}
	//UE_LOG(LogPhysics, Warning, TEXT("WheelAngles: %s"), *AllWheelAngles);
	//UE_LOG(LogPhysics, Warning, TEXT("WheelSpeeds: %s"), *AllWheelSpeeds);
}

float UWheeledVehicleMovementComponentTank::GetLeftTrackSpeed() const
{
	return LeftTrackSpeed;
}

float UWheeledVehicleMovementComponentTank::GetRightTrackSpeed() const
{
	return RightTrackSpeed;
}

void UWheeledVehicleMovementComponentTank::LockRightDrivetrainComponents()
{
	bRightTrackLocked = true;
}

void UWheeledVehicleMovementComponentTank::LockLeftDrivetrainComponents()
{
	bLeftTrackLocked = true;
}

void UWheeledVehicleMovementComponentTank::UnlockRightDrivetrainComponents()
{
	bRightTrackLocked = false;
}

void UWheeledVehicleMovementComponentTank::UnlockLeftDrivetrainComponents()
{
	bLeftTrackLocked = false;
}

float UWheeledVehicleMovementComponentTank::PackFloats(float InUnpackedFloat1, float InUnpackedFloat2)
{
#if PLATFORM_LITTLE_ENDIAN
	const bool bIsLittleEndian = true;
#else
	const bool bIsLittleEndian = false;
#endif

	//Pack the floats
	const unsigned char a = ((unsigned char)(FMath::Clamp(128.0f * (InUnpackedFloat1 + 1.0f) - 1.0f, 0.0f, 255.0f)));
	const unsigned char b = ((unsigned char)(FMath::Clamp(128.0f * (InUnpackedFloat2 + 1.0f) - 1.0f, 0.0f, 255.0f)));
	float OutFloat = 0.0f;
	*((unsigned char*)(&OutFloat) + (bIsLittleEndian ? 0 : 3)) = a;
	*((unsigned char*)(&OutFloat) + (bIsLittleEndian ? 1 : 2)) = b;

	return OutFloat;
}

void UWheeledVehicleMovementComponentTank::UnpackFloats(float InPackedFloat, float& OutUnpackedFloat1, float& OutUnpackedFloat2)
{
#if PLATFORM_LITTLE_ENDIAN
	const bool bIsLittleEndian = true;
#else
	const bool bIsLittleEndian = false;
#endif

	//Unpack the floats
	const unsigned char ua = *((unsigned char*)(&InPackedFloat) + (bIsLittleEndian ? 0 : 3));
	OutUnpackedFloat1 = (((float)ua + 1.0f) / 128.0f) - 1.0f;
	const unsigned char ub = *((unsigned char*)(&InPackedFloat) + (bIsLittleEndian ? 1 : 2));
	OutUnpackedFloat2 = (((float)ub + 1.0f) / 128.0f) - 1.0f;
}