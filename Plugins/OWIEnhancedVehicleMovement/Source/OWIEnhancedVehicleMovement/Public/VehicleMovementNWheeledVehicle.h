// Copyright (c) 2019, Offworld Industries.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "VehicleMovementNWheeledVehicle.generated.h"

/**
 * 
 */
UCLASS()
class AVehicleMovementNWheeledVehicle : public AWheeledVehicle
{
	GENERATED_BODY()

	AVehicleMovementNWheeledVehicle(const FObjectInitializer& ObjectInitializer);
	
};
