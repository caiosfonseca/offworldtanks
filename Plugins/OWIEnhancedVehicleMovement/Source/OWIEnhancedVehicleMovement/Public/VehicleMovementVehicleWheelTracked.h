// Copyright 2015-2016 Offworld Industries Ltd. All Rights Reserved.

#pragma once

#include "VehicleWheel.h"
#include "VehicleMovementVehicleWheelTracked.generated.h"

UCLASS()
class UVehicleMovementWheelTracked : public UVehicleWheel
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Suspension")
	FVector SuspensionDirection;

};
