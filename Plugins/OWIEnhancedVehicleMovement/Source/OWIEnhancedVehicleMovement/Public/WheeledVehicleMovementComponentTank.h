// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

/*
 * Base VehicleSim for the Tank PhysX vehicle class
 */
#pragma once
#include "WheeledVehicleMovementComponent.h"
#include "Curves/CurveFloat.h"
#include "WheeledVehicleMovementComponentTank.generated.h"

USTRUCT()
struct FVehicleTankEngineData
{
	GENERATED_USTRUCT_BODY()

	/** Torque (Nm) at a given RPM*/
	UPROPERTY(EditAnywhere, Category = Setup)
	FRuntimeFloatCurve TorqueCurve;

	/** Maximum revolutions per minute of the engine */
	UPROPERTY(EditAnywhere, Category = Setup, meta = (ClampMin = "0.01", UIMin = "0.01"))
	float MaxRPM;

	/** Moment of inertia of the engine around the axis of rotation (Kgm^2). */
	UPROPERTY(EditAnywhere, Category = Setup, meta = (ClampMin = "0.01", UIMin = "0.01"))
	float MOI;

	/** Damping rate of engine when full throttle is applied (Kgm^2/s) */
	UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float DampingRateFullThrottle;

	/** Damping rate of engine in at zero throttle when the clutch is engaged (Kgm^2/s)*/
	UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float DampingRateZeroThrottleClutchEngaged;

	/** Damping rate of engine in at zero throttle when the clutch is disengaged (in neutral gear) (Kgm^2/s)*/
	UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float DampingRateZeroThrottleClutchDisengaged;

	/** Find the peak torque produced by the TorqueCurve */
	float FindPeakTorque() const;
};

USTRUCT()
struct FVehicleTankGearData
{
	GENERATED_USTRUCT_BODY()

	/** Determines the amount of torque multiplication*/
	UPROPERTY(EditAnywhere, Category = Setup)
	float Ratio;

	/** Value of engineRevs/maxEngineRevs that is low enough to gear down*/
	UPROPERTY(EditAnywhere, meta = (ClampMin = "0.0", UIMin = "0.0", ClampMax = "1.0", UIMax = "1.0"), Category = Setup)
	float DownRatio;

	/** Value of engineRevs/maxEngineRevs that is high enough to gear up*/
	UPROPERTY(EditAnywhere, meta = (ClampMin = "0.0", UIMin = "0.0", ClampMax = "1.0", UIMax = "1.0"), Category = Setup)
	float UpRatio;
};

USTRUCT()
struct FVehicleTankTransmissionData
{
	GENERATED_USTRUCT_BODY()
	/** Whether to use automatic transmission */
	UPROPERTY(EditAnywhere, Category = VehicleSetup, meta=(DisplayName = "Automatic Transmission"))
	bool bUseGearAutoBox;

	/** Time it takes to switch gears (seconds) */
	UPROPERTY(EditAnywhere, Category = Setup, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float GearSwitchTime;

	/** Minimum time it takes the automatic transmission to initiate a gear change (seconds)*/
	UPROPERTY(EditAnywhere, Category = Setup, meta = (editcondition = "bUseGearAutoBox", ClampMin = "0.0", UIMin="0.0"))
	float GearAutoBoxLatency;

	/** The final gear ratio multiplies the transmission gear ratios.*/
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Setup)
	float FinalRatio;

	/** Forward gear ratios (up to 30) */
	UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay)
	TArray<FVehicleTankGearData> ForwardGears;

	UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay)
	TArray<FVehicleTankGearData> BackwardGears;

	/** Reverse gear ratio */
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Setup)
	float ReverseGearRatio;

	/** Value of engineRevs/maxEngineRevs that is high enough to increment gear*/
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Setup, meta = (ClampMin = "0.0", UIMin = "0.0", ClampMax = "1.0", UIMax = "1.0"))
	float NeutralGearUpRatio;
	/** Value of engineRevs/maxEngineRevs that is high enough to increment gear*/
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Setup, meta = (ClampMin = "0.0", UIMin = "0.0", ClampMax = "1.0", UIMax = "1.0"))
	float NeutralGearDownRatio;

	/** Strength of clutch (Kgm^2/s)*/
	UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float ClutchStrength;
};

UCLASS(ClassGroup = (Physics), meta = (BlueprintSpawnableComponent), hidecategories = (PlanarMovement, "Components|Movement|Planar", Activation, "Components|Activation"))
class UWheeledVehicleMovementComponentTank : public UWheeledVehicleMovementComponent
{
	GENERATED_UCLASS_BODY()

	/** Engine */
	UPROPERTY(EditAnywhere, Category = MechanicalSetup)
	FVehicleTankEngineData EngineSetup;

	/** Transmission data */
	UPROPERTY(EditAnywhere, Category = MechanicalSetup)
	FVehicleTankTransmissionData TransmissionSetup;

	virtual void Serialize(FArchive& Ar) override;
	virtual void ComputeConstants() override;
#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	/** When bControlTracksIndividually is enabled, this allows setting the throttle input of both tracks. */
	UFUNCTION(BlueprintCallable, Category = Vehicle)
	virtual void SetBothTracksThrottleInput(float InThrottle);

	/** When bControlTracksIndividually is enabled, this allows setting the throttle input of the left track. */
	UFUNCTION(BlueprintCallable, Category = Vehicle)
	virtual void SetLeftTrackThrottleInput(float InThrottle);

	/** When bControlTracksIndividually is enabled, this allows setting the throttle input of the right track. */
	UFUNCTION(BlueprintCallable, Category = Vehicle)
	virtual void SetRightTrackThrottleInput(float InThrottle);

	/** Pass in two float values to output the speed of the left and right tracks into, this will calculate the speed of each one and set the member variable correctly */
	virtual void CalculateSpeedOfEachTrack(float DeltaTime, float& OutCalcLeftTrackSpeed, float& OutCalcRightTrackSpeed);

	/** Returns the speed (cm/s) of the left track after having called Calculate Speed of Each Track (which occurs during UpdateSimulation) */
	UFUNCTION(BlueprintPure, Category = Vehicle)
	virtual float GetLeftTrackSpeed() const;

	/** Returns the speed (cm/s) of the right track after having called Calculate Speed of Each Track (which occurs during UpdateSimulation) */
	UFUNCTION(BlueprintPure, Category = Vehicle)
	virtual float GetRightTrackSpeed() const;

	virtual void LockRightDrivetrainComponents();

	virtual void LockLeftDrivetrainComponents();

	virtual void UnlockRightDrivetrainComponents();

	virtual void UnlockLeftDrivetrainComponents();

protected:
	/** Allocate and setup the PhysX vehicle */
	virtual void SetupVehicle() override;

	virtual int32 GetCustomGearBoxNumForwardGears() const;
	virtual int32 GetCustomGearBoxNumBackwardGears() const;

	virtual void UpdateSimulation(float DeltaTime) override;

	/** update simulation data: engine */
	virtual void UpdateEngineSetup(const FVehicleTankEngineData& NewEngineSetup);

	/** update simulation data: transmission */
	virtual void UpdateTransmissionSetup(const FVehicleTankTransmissionData& NewGearSetup);

	/** Compress two floats into one float, the two unpacked floats are between -1 and 1 and compressed as bytes. */
	virtual float PackFloats(float InUnpackedFloat1, float InUnpackedFloat2);
	/** Uncompress float into two separate floats between -1 and 1. */
	virtual void UnpackFloats(float InPackedFloat, float& OutUnpackedFloat1, float& OutUnpackedFloat2);

	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	float RawBothTracksThrottleInput;

	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	float RawLeftTrackThrottleInput;

	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	float RawRightTrackThrottleInput;

	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	float BothTracksThrottleInput;

	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	float LeftTrackThrottleInput;

	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	float RightTrackThrottleInput;

	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	TArray<float> WheelSpeeds;

	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	TArray<float> WheelAngles;

	/** Average wheel speed of all left track wheels in cm/s. */
	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	float LeftTrackSpeed;

	/** Average wheel speed of all right track wheels in cm/s. */
	UPROPERTY(BlueprintReadOnly, Category = Vehicle)
	float RightTrackSpeed;

	/** Flag indicating whether the left track of the tank is locked due to being broken */
	bool bLeftTrackLocked;

	/** Flag indicating whether the right track of the tank is locked due to being broken */
	bool bRightTrackLocked;
};