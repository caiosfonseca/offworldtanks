// Copyright (c) 2019, Offworld Industries.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "VehicleMovementTankVehicle.generated.h"

UCLASS()
class AVehicleMovementTankVehicle : public AWheeledVehicle
{
	GENERATED_BODY()

	AVehicleMovementTankVehicle(const FObjectInitializer& ObjectInitializer);
};
