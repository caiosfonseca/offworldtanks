# Vehicle Movement Components

This plugin adds two additional PhysX based movement componets. 
- UWheeledVehicleMovementComponentTank - Designed simulating tank tracks
- WheeledVehicleMovementComponentNW - Designed for simulating any vehicle with over 4 wheels (i.e: LVS)

To use these simply attach the movement component onto your already rigged vehicle. From there you can tweak values and modify the way they drive. Reference the supplied example vehicle in the content folder